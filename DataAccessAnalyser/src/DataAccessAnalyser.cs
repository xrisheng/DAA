﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using Newtonsoft.Json;


namespace DataAccessAnalyser
{
    public class FrameResult
    {
        public FrameResult()
        {
            Name = "";
            ReadSigBit = 0;
            WriteSigBit = 0;
            MaxReadSync = 0;
            MaxWriteSync = 0;
            MaxReadRace = 0;
            MaxWriteRace = 0;
            MaxReadTime = 0;
            MaxWriteTime = 0;
        }

        public string Name { set; get; }
        public double ReadSigBit { set; get; }
        public double WriteSigBit { set; get; }
        public int MaxReadTime { set; get; }
        public int MaxWriteTime { set; get; }
        public int MaxReadSync { set; get; }
        public int MaxWriteSync { set; get; }
        public int MaxWriteRace { set; get; }
        public int MaxReadRace { set; get; }
    }


    public class DataAccessAnalyser
    {
        public DataAccessAnalyser(DataSet dataSet, string outputPath, int execMus = -1)
        {
            Signals = dataSet.Signals;
            OutputPath = outputPath;

            AllFrames = dataSet.TaskFrames;

            //exclude unknown extern frame, init frame and sub_init frame.
            NormalFrames = dataSet.TaskFrames.GetRange(3, dataSet.TaskFrames.Count - 3);


            //partial analysis for seleted time interval [0, execMus]
            if (execMus != -1)
            {
                ExecMus = execMus;
            }
            else
            {
                ExecMus = CalcHyperPeriod();
            }

            //convert execution time from mus to CPU cycle
            ExecCycles = Constants.MusToCycles(ExecMus);

            MCReadDelay = new Dictionary<string, Dictionary<int, int>>();
            MCWriteDelay = new Dictionary<string, Dictionary<int, int>>();

            foreach (TaskFrame frame in NormalFrames)
            {
                MCReadDelay[frame.Name] = new Dictionary<int, int>();
                MCWriteDelay[frame.Name] = new Dictionary<int, int>();
            }

            //initialize variables
            TaskInsts = new Dictionary<string, List<TaskInstance>>();
            Result = new List<FrameResult>();
        }

        private Dictionary<string, List<TaskInstance>> TaskInsts;
        private Dictionary<string, Dictionary<int, int>> MCReadDelay;
        private Dictionary<string, Dictionary<int, int>> MCWriteDelay;

        public List<TaskFrame> AllFrames;
        public List<TaskFrame> InitFrames;
        public List<TaskFrame> NormalFrames;

        public List<Signal> Signals;

        public string OutputPath;
        public List<FrameResult> Result;

        public int CoreNum; // CPU core number
        public int MemSegNum; //memory segment number 
        public int LETNum; // let frame number
        public int SigNum; // signal number
        public int ExecCycles; // execution time calculated in this model, in CPU cycle
        public int ExecMus; // execution time calculated in this model, in mus.

        public int WritePhaseLen;

        //identify if two time interval overlapps, assume both start < end
        private bool IsOverlap(long lStart, long lEnd, long rStart, long rEnd)
        {

            return !(lEnd < rStart || rEnd < lStart);
        }

        //get the hyper period of all LET frame.
        private int CalcHyperPeriod()
        {
            BigInteger bigLCM = new BigInteger(1);
            BigInteger bigGCD;

            foreach (TaskFrame frame in AllFrames)
            {
                if (frame.PeriodMs == 0.0)
                {
                    continue;
                }
                BigInteger bigLeft = new BigInteger(Convert.ToInt32(frame.PeriodMs * 1000));
                bigGCD = BigInteger.GreatestCommonDivisor(bigLeft, bigLCM);
                bigLCM = BigInteger.Divide(BigInteger.Multiply(bigLeft, bigLCM), bigGCD);
                continue;
            }

            int cycle = (int)bigLCM;
            return cycle;
        }

        // Set some constant values, e.g.: CPU core number, maximum delay, memory segment number...
        private void CalcConstant()
        {
            CoreNum = 4;
            LETNum = NormalFrames.Count();
            SigNum = Signals.Count();
            WritePhaseLen = Constants.MusToCycles(50); //length of write phase is 50 mus;
        }


        //Find the last writer IN THE REMOTE COREs!
        private void FindLastWriters(SignalUsage sigu, int cycle, string CoreId, List<TaskInstance> lastWriters, int writeStart, int writeEnd)
        {
            List<TaskFrame> writers = sigu.Signal.Writers;

            foreach (TaskFrame frame in writers)
            {

                if (frame.Type.Contains("INIT") || frame.Type.Contains("EXTERN"))
                {
                    continue;
                }

                if (frame.CoreId == CoreId) // local CPU write can be ignored, as the signal already in DSPR, no extra read is required.
                {
                    continue;
                }

                List<TaskInstance> insts = TaskInsts[frame.Name];
                TaskInstance last = null;

                //find the last instance
                for (int i = insts.Count - 1; i >= 0; i--)
                {
                    if (insts[i].InstEnd < cycle)
                    {
                        last = insts[i];
                        break;
                    }
                }

                //if not found, time is too early, create a dummy instance exists in negative time
                if (last == null)
                {
                    string key = frame.Name + frame.CoreId;
                    last = new TaskInstance(-1, frame, CoreNum);
                }

                if (lastWriters == null)
                {
                    lastWriters.Add(last);
                    writeStart = last.MinWriteStart;
                    writeEnd = last.MaxWriteEnd;
                }
                // the new last instance is overlapp with previous others, extend the write interval
                else if (IsOverlap(writeStart, writeEnd, last.MinWriteStart, last.MaxWriteEnd))
                {
                    lastWriters.Add(last);
                    writeStart = writeStart < last.MinWriteStart ? writeStart : last.MinWriteStart;
                    writeEnd = writeEnd > last.MaxWriteEnd ? writeEnd : last.MaxWriteEnd;
                }
                //the new last instance is not overlapped with prevous others. reset last instances.
                else if (last.MinWriteStart > writeEnd)
                {
                    lastWriters.Clear();
                    lastWriters.Add(last);
                    writeStart = last.MinWriteStart;
                    writeEnd = last.MaxWriteEnd;
                }
            }
        }

        //Find the last reader IN THE LOCAL CORE! 
        private void FindLastReaders(SignalUsage sigu, int cycle, string CoreId, List<TaskInstance> lastReaders, int readStart, int readEnd)
        {
            List<TaskFrame> readers = sigu.Signal.Readers;

            foreach (TaskFrame frame in readers)
            {
                //unknown frame, ignore
                if (frame.Type.Contains("INIT") || frame.Name.Contains("EXTERN"))
                {
                    continue;
                }

                if (frame.CoreId != CoreId) // if the reader is not in local core, ignore
                {
                    continue;
                }

                List<TaskInstance> insts = TaskInsts[frame.Name];
                TaskInstance last = null;

                //find the last instance
                for (int i = insts.Count - 1; i >= 0; i--)
                {
                    if (insts[i].MinReadStart < cycle)
                    {
                        last = insts[i];
                        break;
                    }
                }
                //if not found, create a dummy instance exists in negative time
                if (last == null)
                {
                    string key = frame.Name + frame.CoreId;
                    last = new TaskInstance(-1, frame, CoreNum);
                }

                if (lastReaders.Count == 0)
                {
                    lastReaders.Add(last);
                    readStart = last.MinReadStart;
                    readEnd = last.MaxReadEnd;
                }
                //found the new next reader, and this reader's read interval is not overlapped with previous others
                else if (last.MinReadStart > readEnd)
                {
                    //remove all previous other instance and reset next instance properties
                    lastReaders.Clear();
                    lastReaders.Add(last);
                    readEnd = last.MinReadStart;
                    readEnd = last.MaxReadEnd;
                }
                //found the new next reader, and this reader's read interval is overlapped with previous others
                else if (IsOverlap(readStart, readEnd, last.MinReadStart, last.MaxReadEnd))
                {
                    //add the instance and extend the read interval
                    lastReaders.Add(last);
                    readStart = readStart < last.MinReadStart ? readStart : last.MinReadStart;
                    readEnd = readEnd > last.MaxReadEnd ? readEnd : last.MaxReadEnd;
                }

            }
        }

        // Find the next writers IN ALL COREs
        private void FindNextWriters(SignalUsage sigu, int cycle, List<TaskInstance> nextWriters, int writeStart, int writeEnd)
        {
            List<TaskFrame> writers = sigu.Signal.Writers;

            foreach (TaskFrame frame in writers)
            {
                if (frame.Type.Contains("INIT") || frame.Type.Contains("EXTERN"))
                {
                    continue;
                }

                List<TaskInstance> insts = TaskInsts[frame.Name];
                TaskInstance next = null;

                for (int i = 0; i < insts.Count; i++)
                {
                    if (insts[i].MaxWriteEnd > cycle)
                    {
                        next = insts[i];
                        break;
                    }
                }

                //if not found, time is too late, create a dummy instance exists in future time
                if (next == null)
                {
                    string key = frame.Name + frame.CoreId;
                    int time = Constants.CyclesToMus(cycle);

                    int period = Convert.ToInt32(frame.PeriodMs * 1000);
                    int offset = Convert.ToInt32(frame.OffsetMs * 1000);

                    int nextInstIndex = (int)((time - offset - 1) / period) + 1;
                    next = new TaskInstance(nextInstIndex, frame, CoreNum);
                }

                if (nextWriters.Count == 0)
                {
                    nextWriters.Add(next);
                    writeStart = next.MinWriteStart;
                    writeEnd = next.MaxWriteEnd;
                }
                //the new instance is not overlapped with prevous others. reset last instances.
                else if (next.MaxWriteEnd < writeStart)
                {
                    nextWriters.Clear();
                    nextWriters.Add(next);
                    writeStart = next.MinWriteStart;
                    writeEnd = next.MaxWriteEnd;
                }
                // the new  instance is overlapp with previous others, extend the write interval
                else if (IsOverlap(writeStart, writeEnd, next.MinWriteStart, next.MaxWriteEnd))
                {
                    nextWriters.Add(next);
                    writeStart = writeStart < next.MinWriteStart ? writeStart : next.MinWriteStart;
                    writeEnd = writeEnd > next.MaxWriteEnd ? writeEnd : next.MaxWriteEnd;
                }

            }
        }

        // Find the next readers IN REMOTE COREs
        private void FindNextReaders(SignalUsage sigu, int cycle, string CoreId, List<TaskInstance> nextReaders, int readStart, int readEnd)
        {
            List<TaskFrame> readers = sigu.Signal.Readers;

            foreach (TaskFrame frame in readers)
            {
                if (frame.Type.Contains("INIT") || frame.Type.Contains("EXTERN"))
                {
                    continue;
                }


                if (frame.CoreId == CoreId) // if the reader is in local core, ignore
                {
                    continue;
                }

                List<TaskInstance> insts = TaskInsts[frame.Name];
                TaskInstance next = null;

                for (int i = 0; i < insts.Count; i++)
                {
                    if (insts[i].MinReadStart > cycle)
                    {
                        next = insts[i];
                        break;
                    }
                }

                if (next == null) //create a dummy instance exists in future hyper-period
                {
                    string key = frame.Name + frame.CoreId;
                    int time = Constants.CyclesToMus(cycle);
                    int period = Convert.ToInt32(frame.PeriodMs * 1000);
                    int offset = Convert.ToInt32(frame.OffsetMs * 1000);
                    int nextInstIndex = (int)((time - offset - 1) / period) + 1;

                    next = new TaskInstance(nextInstIndex, frame, CoreNum);
                }

                if (nextReaders.Count == 0)
                {
                    nextReaders.Add(next);
                    readStart = next.MinReadStart;
                    readEnd = next.MaxReadEnd;
                }
                //the new instance is not overlapped with prevous others. reset last instances.
                else if (next.MaxReadEnd < readStart)
                {
                    nextReaders.Clear();
                    nextReaders.Add(next);
                    readStart = next.MinReadStart;
                    readEnd = next.MaxReadEnd;
                }
                // the new  instance is overlapp with previous others, extend the read interval
                else if (IsOverlap(readStart, readEnd, next.MinReadStart, next.MaxReadEnd))
                {
                    nextReaders.Add(next);
                    readStart = readStart < next.MinReadStart ? readStart : next.MinReadStart;
                    readEnd = readEnd > next.MaxReadEnd ? readEnd : next.MaxReadEnd;
                }
            }
        }

        //Generate Task instance
        private void SetTaskInst()
        {
            int letIndex = 0;
            int letCnt = 0;
            foreach (TaskFrame frame in NormalFrames)
            {
                List<TaskInstance> lst = new List<TaskInstance>();
                int period = Convert.ToInt32(frame.PeriodMs * 1000);
                int offset = Convert.ToInt32(frame.OffsetMs * 1000);
                int instNum = (int)((ExecMus - offset - 1) / period) + 1;
                for (int i = 0; i < instNum; i++)
                {
                    string key = frame.Name + frame.CoreId;
                    TaskInstance inst = new TaskInstance(i, frame, CoreNum);
                    letCnt++;
                    lst.Add(inst);
                }
                letIndex++;
                TaskInsts[frame.Name] = lst;
            }
            Logger.Log("Data Access Analyser: In total " + letCnt + " LET instances are generated.");
        }

        //strategy 1: buffer all the related signals
        private void SetSignalInst1()
        {
            int gReadSigIndex = 0;
            int gWriteSigIndex = 0;

            foreach (TaskFrame frame in NormalFrames)
            {
                for (int j = 0; j < TaskInsts[frame.Name].Count(); j++)
                {
                    int k_r = 0;
                    int k_w = 0;

                    foreach (SignalUsage sigu in frame.AllInputs)
                    {

                        SignalInstance inst = new SignalInstance(sigu, j, k_r, gReadSigIndex, TaskInsts[frame.Name][j].CPUIndex);
                        TaskInsts[frame.Name][j].ReadSignals.Add(inst);
                        k_r++;
                        gReadSigIndex++;
                    }

                    foreach (SignalUsage sigu in frame.AllOutputs)
                    {
                        SignalInstance inst = new SignalInstance(sigu, j, k_w, gWriteSigIndex, TaskInsts[frame.Name][j].CPUIndex);
                        TaskInsts[frame.Name][j].WriteSignals.Add(inst);
                        k_w++;
                        gWriteSigIndex++;

                    }
                }
            }
            Logger.Log("Data Access Analyser: In total " + gWriteSigIndex + " write signal instance are generated.");
            Logger.Log("Data Access Analyser: In total " + gReadSigIndex + " read signal instance are generated.");
        }

        //strategy 2: buffer only the necessary signals
        private void SetSignalInst2()
        {
            int gReadSigIndex = 0;
            int gWriteSigIndex = 0;

            foreach (TaskFrame frame in NormalFrames)
            {
                for (int j = 0; j < TaskInsts[frame.Name].Count(); j++)
                {
                    TaskInsts[frame.Name][j].ReadSignals.Clear();
                    TaskInsts[frame.Name][j].WriteSignals.Clear();

                    foreach (SignalUsage sigu in frame.AllInputs)
                    {
                        int k = 0;
                        TaskInstance cur = TaskInsts[frame.Name][j];
                        int now = cur.MinReadStart;

                        bool flag = true;

                        if (sigu.Signal.Writers.Count == 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            List<TaskInstance> lr = new List<TaskInstance>();
                            List<TaskInstance> lw = new List<TaskInstance>();
                            int readStart = 0;
                            int readEnd = 0;
                            int writeStart = 0;
                            int writeEnd = 0;

                            FindLastReaders(sigu, now, frame.CoreId, lr, readStart, readEnd);
                            FindLastWriters(sigu, now, frame.CoreId, lw, writeStart, writeEnd);

                            if (lr.Count == 0 || lw.Count == 0) //unknown signal flow
                            {
                                flag = true;
                            }
                            //the signal already buffered by previous reader in the same core, and the reader's read interval is not overlapp with current time point
                            else if (readStart > writeEnd && now > readEnd)
                            {
                                flag = false;
                            }
                            else
                            {
                                flag = true;
                            }
                        }

                        if (flag)
                        {
                            SignalInstance inst = new SignalInstance(sigu, j, k, gReadSigIndex, TaskInsts[frame.Name][j].CPUIndex);
                            TaskInsts[frame.Name][j].ReadSignals.Add(inst);
                            k++;
                            gReadSigIndex++;
                        }
                    }

                    foreach (SignalUsage sigu in frame.AllOutputs)
                    {
                        int k = 0;
                        TaskInstance cur = TaskInsts[frame.Name][j];
                        int now = cur.MinWriteStart;

                        bool flag = false;

                        if (sigu.Signal.Readers.Count == 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            List<TaskInstance> nr = new List<TaskInstance>();
                            List<TaskInstance> nw = new List<TaskInstance>();
                            int readStart = 0;
                            int readEnd = 0;
                            int writeStart = 0;
                            int writeEnd = 0;

                            FindNextReaders(sigu, now, frame.CoreId, nr, readStart, readEnd);
                            FindNextWriters(sigu, now, nw, writeStart, writeEnd);

                            if (nr == null || nw == null)
                            {
                                flag = true;
                            }
                            //the signal will be overwritten by next writer before any of the reader comes, the the writer's write interval is not overlapped with current time point
                            else if (writeEnd < readStart && now < writeStart)
                            {
                                flag = false;
                            }
                            else
                            {
                                flag = true;
                            }
                        }

                        if (flag)
                        {
                            SignalInstance inst = new SignalInstance(sigu, j, k, gWriteSigIndex, TaskInsts[frame.Name][j].CPUIndex);
                            TaskInsts[frame.Name][j].WriteSignals.Add(inst);
                            k++;
                            gWriteSigIndex++;
                        }
                    }
                }
            }

            Logger.Log("Data Access Analyser: In total " + gReadSigIndex + " read signal instances are generated.");
            Logger.Log("Data Access Analyser: In total " + gWriteSigIndex + " write signal instances are generated.");
        }

        private void InitTaskInst()
        {
            foreach (TaskFrame frame in NormalFrames)
            {
                for (int j = 0; j < TaskInsts[frame.Name].Count(); j++)
                {
                    if (frame.Type == "BET")
                    {
                        TaskInsts[frame.Name][j].MinReadStart = TaskInsts[frame.Name][j].InstStart;
                        TaskInsts[frame.Name][j].MaxReadEnd = TaskInsts[frame.Name][j].InstEnd;
                        TaskInsts[frame.Name][j].MinWriteStart = TaskInsts[frame.Name][j].InstStart;
                        TaskInsts[frame.Name][j].MaxWriteEnd = TaskInsts[frame.Name][j].InstEnd;
                    }
                    else
                    {
                        TaskInsts[frame.Name][j].MaxReadTimeCost = TaskInsts[frame.Name][j].InstEnd - TaskInsts[frame.Name][j].InstStart - WritePhaseLen;
                        TaskInsts[frame.Name][j].MaxWriteTimeCost = WritePhaseLen - TaskInsts[frame.Name][j].WriteStartDelay;
                        TaskInsts[frame.Name][j].ReadISRDelay = Constants.MusToCycles(10);
                        TaskInsts[frame.Name][j].WriteISRDelay = Constants.MusToCycles(10);
                        TaskInsts[frame.Name][j].ReadStartDelay = Constants.MusToCycles(5);
                        TaskInsts[frame.Name][j].WriteStartDelay = Constants.MusToCycles(5);

                        TaskInsts[frame.Name][j].MinReadStart = TaskInsts[frame.Name][j].InstStart + TaskInsts[frame.Name][j].ReadStartDelay;
                        TaskInsts[frame.Name][j].MaxReadEnd = TaskInsts[frame.Name][j].InstEnd;
                        TaskInsts[frame.Name][j].MinWriteStart = TaskInsts[frame.Name][j].InstEnd - WritePhaseLen + TaskInsts[frame.Name][j].WriteStartDelay;
                        TaskInsts[frame.Name][j].MaxWriteEnd = TaskInsts[frame.Name][j].InstEnd;

                    }
                }
            }
        }

        //The neighbors are the other task instance who have overlapped operations
        private void FindTaskInstNeighbors()
        {
            foreach (TaskFrame frame in NormalFrames)
            {
                Dictionary<string, int> curMatchStart = new Dictionary<string, int>();

                foreach (TaskFrame item in NormalFrames)
                {
                    curMatchStart[item.Name] = 0;
                }

                for (int j = 0; j < TaskInsts[frame.Name].Count(); j++)
                {
                    long l_LETStart = TaskInsts[frame.Name][j].InstStart;
                    long l_LETEnd = TaskInsts[frame.Name][j].InstEnd;

                    foreach (TaskFrame other in NormalFrames)
                    {
                        //skip if two instance are from same cpu core
                        if (frame.CoreId == other.CoreId) { continue; }

                        //start to comparison
                        for (int n = 0; n < TaskInsts[other.Name].Count(); n++)
                        {
                            long r_LETStart = TaskInsts[other.Name][n].InstStart;
                            long r_LETEnd = TaskInsts[other.Name][n].InstEnd;

                            //-------------------------------------------------------------
                            //                |----------|
                            //                |l_instance|
                            //                |----------|
                            // |----------|
                            // |r_instance|
                            // |----------|
                            // ------------------------------------------------------------> TIME
                            if (r_LETEnd <= l_LETStart) //right LET instance is too early
                            {
                                // both instance sequence are ascending ordered in time
                                // for next instance of l_instance, we can start comparison from next instance of r_instance
                                curMatchStart[other.Name]++;
                                continue;
                            }
                            //-------------------------------------------------------------
                            //                |----------|
                            //                |l_instance|
                            //                |----------|
                            //                              |----------|
                            //                              |r_instance|
                            //                              |----------|
                            // ------------------------------------------------------------> TIME
                            else if (l_LETEnd <= r_LETStart)//right LET instance is too later
                            {
                                break;// as both instance sequence are ascending ordered in time, no need the compare with further instance
                            }
                            //-------------------------------------------------------------
                            //                |----------|
                            //                |l_instance|
                            //                |----------|
                            //            |----------|
                            //            |r_instance|
                            //            |----------|
                            // ------------------------------------------------------------> TIME
                            else// two instance overlapp
                            {
                                long l_ReadStart = TaskInsts[frame.Name][j].MinReadStart;
                                long l_ReadEnd = TaskInsts[frame.Name][j].MaxReadEnd;
                                long l_WriteStart = TaskInsts[frame.Name][j].MinWriteStart;
                                long l_WriteEnd = TaskInsts[frame.Name][j].MaxWriteEnd;
                                int l_CPUIndex = TaskInsts[frame.Name][j].CPUIndex;

                                long r_ReadStart = TaskInsts[other.Name][n].MinReadStart;
                                long r_ReadEnd = TaskInsts[other.Name][n].MaxReadEnd;
                                long r_WriteStart = TaskInsts[other.Name][n].MinWriteStart;
                                long r_WriteEnd = TaskInsts[other.Name][n].MaxWriteEnd;
                                int r_CPUIndex = TaskInsts[other.Name][n].CPUIndex;

                                if (IsOverlap(l_ReadStart, l_ReadEnd, r_ReadStart, r_ReadEnd))
                                {
                                    TaskInsts[frame.Name][j].ReadReadNeighbors[r_CPUIndex].Add(TaskInsts[other.Name][n]);
                                }

                                if (IsOverlap(l_WriteStart, l_WriteEnd, r_ReadStart, r_ReadEnd))
                                {
                                    TaskInsts[frame.Name][j].WriteReadNeighbors[r_CPUIndex].Add(TaskInsts[other.Name][n]);
                                }

                                if (IsOverlap(l_ReadStart, l_ReadEnd, r_WriteStart, r_WriteEnd))
                                {
                                    TaskInsts[frame.Name][j].ReadWriteNeighbors[r_CPUIndex].Add(TaskInsts[other.Name][n]);
                                }

                                if (IsOverlap(l_WriteStart, l_WriteEnd, r_WriteStart, r_WriteEnd))
                                {
                                    TaskInsts[frame.Name][j].WriteWriteNeighbors[r_CPUIndex].Add(TaskInsts[other.Name][n]);
                                }
                            }
                        }
                    }
                }
            }
        }

        //sync delay is the delay that read start until all write operations are done, vice visa.
        private void CalcMaxSyncDelay()
        {
            foreach (TaskFrame frame in NormalFrames)
            {
                if (frame.Type == "BET")
                {
                    // the BET task has always lower priority than LET task, thus no sync delay
                    continue;
                }

                for (int j = 0; j < TaskInsts[frame.Name].Count(); j++)
                {
                    List<int> maxWriteSync = new List<int>();
                    List<int> maxReadSync = new List<int>();

                    for (int l = 0; l < CoreNum; l++)
                    {
                        int maxWriteLast = 0;
                        int maxReadLast = 0;

                        //calculate the maximum overlapped interval in every signal core
                        foreach (TaskInstance neighbor in TaskInsts[frame.Name][j].ReadWriteNeighbors[l])
                        {

                            int tmp = neighbor.MaxWriteEnd - TaskInsts[frame.Name][j].MinReadStart;
                            tmp = tmp > neighbor.MaxWriteTimeCost ? neighbor.MaxWriteTimeCost : tmp;
                            maxWriteLast = tmp > maxWriteLast ? tmp : maxWriteLast;
                        }
                        maxReadSync.Add(maxWriteLast);

                        //calculate the maximum overlapped interval in every signal core
                        foreach (TaskInstance neighbor in TaskInsts[frame.Name][j].WriteReadNeighbors[l])
                        {
                            int tmp = neighbor.MaxReadEnd - TaskInsts[frame.Name][j].MinWriteStart;
                            tmp = tmp > neighbor.MaxReadTimeCost ? neighbor.MaxReadTimeCost : tmp;
                            maxReadLast = tmp > maxReadLast ? tmp : maxReadLast;
                        }
                        maxWriteSync.Add(maxReadLast);
                    }

                    //overestimate a little: the sum of the overlapped interval in every core is the max sync delay 
                    TaskInsts[frame.Name][j].MaxReadSyncDelay = maxReadSync.Sum();
                    TaskInsts[frame.Name][j].MaxWriteSyncDelay = maxWriteSync.Sum();
                }
            }
        }

        //generate simulation sequence for targe instance "me"
        private List<int> GenSimuSeqMe(TaskInstance me, string type)
        {
            List<int> meSigEvent = new List<int>();
            int curTime = 0;
            Random rnd = new Random(Guid.NewGuid().GetHashCode());

            List<SignalInstance> sigis;

            // determine the generated sequence type
            if (type == "read")
            {
                sigis = me.ReadSignals;
            }
            else
            {
                sigis = me.WriteSignals;
            }


            foreach (SignalInstance sig in sigis)
            {
                // on average the read operation requires more extra cycle because the memory continuality of write operation is better
                if (type == "read")
                {
                    curTime += rnd.Next(5, 20);
                }
                else
                {
                    curTime += rnd.Next(0, 5);
                }


                int accessStall;
                if (type == "read")
                {
                    accessStall = 7;
                }
                else
                {
                    accessStall = 5;
                }
                int dataLen = sig.Sigu.Signal.Dim * sig.Sigu.Signal.Size;
                int txNr = (dataLen / 256) + 1;

                //other transder mode
                for (int i = 0; i < txNr; i++)
                {
                    meSigEvent.Add(curTime);
                    curTime += accessStall + 2;
                }
            }
            return meSigEvent;
        }

        //generate the simulation sequence for the neighbors of target instance
        private List<List<int>> GenSimuSeqNeighbor(TaskInstance me, string type)
        {
            //generate sequence per core
            List<List<int>> neighborSigEvent = new List<List<int>>();
            for (int i = 0; i < CoreNum; i++)
            {
                List<int> tmp = new List<int>();
                neighborSigEvent.Add(tmp);
            }

            Random rnd = new Random(Guid.NewGuid().GetHashCode());

            //decide which type of sequence
            List<List<TaskInstance>> neighbors;
            int accessStall;
            if (type == "read")
            {
                neighbors = me.ReadReadNeighbors;
                accessStall = 7;
            }
            else
            {
                neighbors = me.WriteWriteNeighbors;
                accessStall = 5;
            }

            //iterating cores
            for (int i = 0; i < CoreNum; i++)
            {
                List<TaskInstance> neighborsInCore = neighbors[i];
                int curTime = 0;

                foreach (TaskInstance inst in neighborsInCore)
                {
                    List<SignalInstance> sigis;
                    if (type == "read")
                    {
                        sigis = inst.ReadSignals;
                        curTime += rnd.Next(5, 20);
                    }
                    else
                    {
                        sigis = inst.WriteSignals;
                        curTime += rnd.Next(0, 5);
                    }

                    foreach (SignalInstance sig in sigis)
                    {

                        int dataLen = sig.Sigu.Signal.Dim * sig.Sigu.Signal.Size;
                        int txNr = (dataLen / 256) + 1;

                        for (int k = 0; k < txNr; k++)
                        {
                            neighborSigEvent[i].Add(curTime);
                            curTime += accessStall + 2;
                        }
                    }
                }
            }
            return neighborSigEvent;
        }

        private void SimuContentionSeq(TaskInstance me, string type, ref int time, ref int race_delay)
        {
            List<int> meSeq = GenSimuSeqMe(me, type);
            List<List<int>> neighborSeq = GenSimuSeqNeighbor(me, type);
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            int accessStall;

            if (type == "read")
            {
                accessStall = 7;
            }
            else
            {
                accessStall = 5;
            }

            if (meSeq.Count == 0)
            {
                return;
            }

            List<int> meStart = new List<int>();
            List<int> meEnd = new List<int>();

            for (int i = 0; i < meSeq.Count; i++)
            {
                meStart.Add(meSeq[i]);
                meEnd.Add(meSeq[i]+ accessStall);
            }
            int oldEnd = meEnd[meEnd.Count - 1];

            foreach (List<int> seq in neighborSeq)
            {
                if (seq.Count == 0)
                {
                    continue;
                }

                List<int> neighborStart = new List<int>();
                List<int> neighborEnd = new List<int>();

                for (int i = 0; i < seq.Count; i++)
                {
                    neighborStart.Add(seq[i]);
                    neighborEnd.Add(seq[i] + accessStall);
                }

                bool flag = meEnd[meEnd.Count - 1] < neighborEnd[neighborEnd.Count - 1];
                int offset = rnd.Next(0, Math.Abs(meEnd[meEnd.Count - 1] - neighborEnd[neighborEnd.Count - 1]));

                List<int> leftStart ;
                List<int> leftEnd ;
                List<int> rightStart;
                List<int> rightEnd ;

                if (flag)
                {
                    leftStart = meStart;
                    leftEnd = meEnd;
                    rightStart = neighborStart;
                    rightEnd = neighborEnd;
                }
                else
                {
                    leftStart = neighborStart;
                    leftEnd = neighborEnd;
                    rightStart = meStart;
                    rightEnd = meEnd;
                }

                //as left value is smaller we add a offset as a random shift of the left sequence, to produce different overlaping case.
                int rIdx = 0;
                int lIdx = 0;

                //iter left sequence
                for (int i = lIdx; i < leftStart.Count; i++)
                {
                    //iter right sequence
                    for (int j = rIdx; j < rightStart.Count; j++)
                    {
                        //left is too early, go to next left
                        if (leftEnd[i]+offset <= rightStart[j])
                        {
                            i++;
                            break;
                        }
                        //left is too late, go to next right
                        else if (leftStart[i]+offset >= rightEnd[j])
                        {
                            rIdx = j;
                            continue;
                        }
                        //overlap and left comes later
                        else if(leftStart[i] + offset > rightStart[j] && leftStart[i] + offset < rightEnd[j])
                        {
                            int overlap = rightEnd[j] - leftStart[i] - offset;

                            leftEnd[i] += overlap;
                            for (int k = i+1; k < leftStart.Count; k++)
                            {
                                leftStart[k] += overlap;
                                leftEnd[k] += overlap;
                            }
                        }
                        //overlap and right comes laters
                        else if(rightStart[j] > leftStart[i] + offset && rightStart[j] < leftEnd[i] + offset)
                        {
                            int overlap = leftEnd[i] + offset - rightStart[j];

                            rightEnd[j] += overlap;

                            for(int k = j+1; k< rightStart.Count; k++)
                            {
                                rightStart[k] += overlap;
                                rightEnd[k] += overlap;
                            }
                        }
                        //overlap and both come same time
                        else
                        {
                            if (flag)
                            {
                                leftEnd[i] += accessStall;
                                for (int k = i + 1; k < leftStart.Count; k++)
                                {
                                    leftStart[k] += accessStall;
                                    leftEnd[k] += accessStall;
                                }
                            }
                            else
                            {
                                rightEnd[j] += accessStall;
                                for (int k = j + 1; k < rightStart.Count; k++)
                                {
                                    rightStart[k] += accessStall;
                                    rightEnd[k] += accessStall;
                                }
                            }
                        }
                    }
                }
            }

            time = meEnd[meEnd.Count - 1];
            race_delay = meEnd[meEnd.Count - 1] - oldEnd;

            if (race_delay > 1000)
            {
                // Logger.Log("LET Instance " + me.FrameName + " " + me.InstIndex + " has race condition delay " + race_delay);
            }
        }

        private void CalcAccessTime()
        {
            foreach (TaskFrame frame in NormalFrames)
            {
                for (int j = 0; j < TaskInsts[frame.Name].Count(); j++)
                {
                    int readMax = 0;
                    int writeMax = 0;
                    int rraceMax = 0;
                    int wraceMax = 0;

                    for (int k = 0; k < 100; k++)
                    {
                        int readVal = 0;
                        int writeVal = 0;
                        int readRace = 0;
                        int writeRace = 0;

                        SimuContentionSeq(TaskInsts[frame.Name][j], "read", ref readVal, ref readRace);
                        SimuContentionSeq(TaskInsts[frame.Name][j], "write", ref writeVal, ref writeRace);
                        readMax = readMax > readVal ? readMax : readVal;
                        writeMax = writeMax > writeVal ? writeMax : writeVal;
                        rraceMax = rraceMax > readRace ? rraceMax : readRace;
                        wraceMax = wraceMax > writeRace ? wraceMax : writeRace;

                        if (MCReadDelay[frame.Name].ContainsKey(readRace))
                        {
                            MCReadDelay[frame.Name][readRace] += 1;
                        }
                        else
                        {
                            MCReadDelay[frame.Name][readRace] = 1;
                        }

                        if (MCWriteDelay[frame.Name].ContainsKey(writeRace))
                        {
                            MCWriteDelay[frame.Name][writeRace] += 1;
                        }
                        else
                        {
                            MCWriteDelay[frame.Name][writeRace] = 1;
                        }
                    }

                    TaskInsts[frame.Name][j].MaxReadTimeCost = readMax + 900; // 900 is the minimum constant time for data accessing
                    TaskInsts[frame.Name][j].MaxWriteTimeCost = writeMax + 900; // 900 is the minimum constant time for data accessing
                    TaskInsts[frame.Name][j].MaxReadRaceDelay = rraceMax;
                    TaskInsts[frame.Name][j].MaxWriteRaceDelay = wraceMax;
                    TaskInsts[frame.Name][j].MaxReadEnd = TaskInsts[frame.Name][j].MinReadStart + TaskInsts[frame.Name][j].ReadISRDelay + TaskInsts[frame.Name][j].MaxReadTimeCost;
                    TaskInsts[frame.Name][j].MaxWriteEnd = TaskInsts[frame.Name][j].MinWriteStart + TaskInsts[frame.Name][j].WriteISRDelay + TaskInsts[frame.Name][j].MaxWriteTimeCost;
                }
            }
        }

        //estimate the SRI delay for let
        public void OutputResult()
        {
            Logger.Log("Output Result for LET frames.");
            foreach (TaskFrame frame in NormalFrames)
            {
                if (frame.Type == "BET")
                {
                    continue;
                }

                FrameResult fRes = new FrameResult();
                fRes.Name = frame.Name;

                for (int j = 0; j < TaskInsts[frame.Name].Count(); j++)
                {
                    fRes.MaxReadTime = fRes.MaxReadTime > TaskInsts[frame.Name][j].MaxReadTimeCost ? fRes.MaxReadTime : TaskInsts[frame.Name][j].MaxReadTimeCost;
                    fRes.MaxWriteTime = fRes.MaxWriteTime > TaskInsts[frame.Name][j].MaxWriteTimeCost ? fRes.MaxWriteTime : TaskInsts[frame.Name][j].MaxWriteTimeCost;
                    fRes.MaxReadSync = fRes.MaxReadSync > TaskInsts[frame.Name][j].MaxReadSyncDelay ? fRes.MaxReadSync : TaskInsts[frame.Name][j].MaxReadSyncDelay;
                    fRes.MaxWriteSync = fRes.MaxWriteSync > TaskInsts[frame.Name][j].MaxWriteSyncDelay ? fRes.MaxWriteSync : TaskInsts[frame.Name][j].MaxWriteSyncDelay;
                    fRes.MaxReadRace = fRes.MaxReadRace > TaskInsts[frame.Name][j].MaxReadRaceDelay ? fRes.MaxReadRace : TaskInsts[frame.Name][j].MaxReadRaceDelay;
                    fRes.MaxWriteRace = fRes.MaxWriteRace > TaskInsts[frame.Name][j].MaxWriteRaceDelay ? fRes.MaxWriteRace : TaskInsts[frame.Name][j].MaxWriteRaceDelay;
                }
                int readSum = 0;
                int writeSum = 0;
                int readBit = 0;
                int writeBit = 0;

                for (int k = 0; k < TaskInsts[frame.Name][0].ReadSignals.Count; k++)
                {
                    readSum += TaskInsts[frame.Name][0].ReadSignals[k].Sigu.Signal.Dim;
                    readBit += TaskInsts[frame.Name][0].ReadSignals[k].Sigu.Signal.Dim * TaskInsts[frame.Name][0].ReadSignals[k].Sigu.Signal.Size;
                }

                for (int k = 0; k < TaskInsts[frame.Name][0].WriteSignals.Count; k++)
                {
                    writeSum += TaskInsts[frame.Name][0].WriteSignals[k].Sigu.Signal.Dim;
                    writeBit += TaskInsts[frame.Name][0].WriteSignals[k].Sigu.Signal.Dim * TaskInsts[frame.Name][0].WriteSignals[k].Sigu.Signal.Size;
                }


                fRes.ReadSigBit = readBit;
                fRes.WriteSigBit = writeBit;
                Result.Add(fRes);

            }
            string EstmationPath = OutputPath + "MOET";
            EstmationPath += "__" + DateTime.Now.ToString("yyyyMMddTHHmmss") + @".csv";

            using (var w = new StreamWriter(EstmationPath))
            {
                var line = "Frame Name;Read-in Bit;Read-in Sync Delay;Read-in Race Delay;Read-in Time Modelled;" +
                                     "Frame Name;Write-out Bit;Write-out Sync Delay;Write-out Race Delay;Write-out Time Modelled;";
                w.WriteLine(line);
                w.Flush();

                foreach (FrameResult item in Result)
                {
                    line = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};",
                                          item.Name, item.ReadSigBit, item.MaxReadSync, item.MaxReadRace, item.MaxReadTime,
                                          item.Name, item.WriteSigBit, item.MaxWriteSync, item.MaxWriteRace, item.MaxWriteTime);

                    w.WriteLine(line);
                    w.Flush();
                }

            }
        }

        public void OutputDistribution(string frameName, string type)
        {
            if (type == "read")
            {
                string ReadPath = OutputPath + "_DelayEstimation_Read_" + frameName;
                ReadPath += "__" + DateTime.Now.ToString("yyyyMMddTHHmmss") + @".csv";
                Dictionary<int, int> readRes = MCReadDelay[frameName];
                readRes.OrderBy(s => s.Key);

                using (var w = new StreamWriter(ReadPath))
                {
                    var line = "Delay;Frequency;";
                    w.WriteLine(line);
                    w.Flush();

                    foreach (KeyValuePair<int, int> pair in readRes)
                    {
                        line = string.Format("{0};{1};",
                                            pair.Key, pair.Value);
                        w.WriteLine(line);
                        w.Flush();
                    }
                }

            }
            else
            {

                string WritePath = OutputPath + "_DelayEstimation_Write_" + frameName + "_";
                WritePath += "__" + DateTime.Now.ToString("yyyyMMddTHHmmss") + @".csv";
                Dictionary<int, int> writeRes = MCWriteDelay[frameName];
                writeRes.OrderBy(s => s.Key);

                using (var w = new StreamWriter(WritePath))
                {
                    var line = "Delay;Frequency;";
                    w.WriteLine(line);
                    w.Flush();

                    foreach (KeyValuePair<int, int> pair in writeRes)
                    {
                        line = string.Format("{0};{1};",
                                            pair.Key, pair.Value);
                        w.WriteLine(line);
                        w.Flush();
                    }
                }

            }

        }

        public void Run()
        {

            CalcConstant();
            SetTaskInst();
            SetSignalInst2();
            InitTaskInst();

            for (int i = 0; i < 3; i++)
            {
                FindTaskInstNeighbors();
                CalcMaxSyncDelay();
                CalcAccessTime();
            }

            OutputResult();
            OutputDistribution("Frame_3", "read");
        }
    }
}
