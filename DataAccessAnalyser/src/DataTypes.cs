﻿using ASAM.MDX;
using System.Collections.Generic;
using System;
using DataAccessAnalyser;

namespace DataAccessAnalyser
{

    public class DataSet
    {
        public DataSet()
        {
            TaskFrames = new List<TaskFrame>();
            Signals = new List<Signal>();
        }

        public DataSet(List<TaskFrame> frames,  List<Signal> signals)
        {
            TaskFrames = frames;
            Signals = signals;
        }

        public List<TaskFrame> TaskFrames { get; }
        public List<Signal> Signals { get; }
    }

    public class TaskFrameSerial
    {
        public TaskFrameSerial()
        {

        }

        public TaskFrameSerial(TaskFrame frame)
        {
            Name = "Frame " + frame.Index.ToString();
            Index = frame.Index;
            CoreId = frame.CoreId;
            Type = frame.Type;
            OffsetMs = frame.OffsetMs;
            PeriodMs = frame.PeriodMs;
            LengthMs = frame.LengthMs;

            AllInputs = new List<string>();
            foreach (SignalUsage su in frame.AllInputs)
            {
                AllInputs.Add("Signal " + su.Signal.Index.ToString());
            }

            AllOutputs = new List<string>();
            foreach (SignalUsage su in frame.AllOutputs)
            {
                AllOutputs.Add("Signal " + su.Signal.Index.ToString());
            }
        }

        public string Name { set; get;  }
        public int Index { set; get; }
        public string CoreId { set; get; }
        public string Type { set; get; }

        public double OffsetMs { set; get; }
        public double PeriodMs { set; get; }
        public double LengthMs { set; get; }

        public List<string> AllInputs { set; get; }
        public List<string> AllOutputs { set; get; }
    }

    public class SignalSerial
    {
        public SignalSerial()
        { }
        public SignalSerial(Signal sig)
        {
            Name = "Signal " + sig.Index.ToString();
            Readers = new List<string>();
            Writers = new List<string>();
            Dim = sig.Dim;
            Size = sig.Size;
            Index = sig.Index;

            foreach (TaskFrame tf in sig.Readers)
            {
                Readers.Add("Frame " + tf.Index.ToString());
            }

            foreach (TaskFrame tf in sig.Writers)
            {
                Writers.Add("Frame " + tf.Index.ToString());
            }

        }

        public string Name { set; get; }
        public int Dim { set; get; }
        public int Size { set; get; }
        public int Index { set; get; }
        public List<string> Readers;
        public List<string> Writers;
    }

    public class TaskFrame
    {
        public TaskFrame(string name)
        {
            Name = name;
            CoreId = "";
            Type = "";
            OffsetMs = 0.0d;
            PeriodMs = 0.0d;
            LengthMs = 0.0d;
            AllOutputs = new List<SignalUsage>();
            AllInputs = new List<SignalUsage>();
        }

        public TaskFrame(TaskFrameSerial ts)
        {
            Name = ts.Name;
            CoreId = ts.CoreId;
            Type = ts.Type;
            OffsetMs = ts.OffsetMs;
            PeriodMs = ts.PeriodMs;
            LengthMs = ts.LengthMs;
            AllOutputs = new List<SignalUsage>();
            AllInputs = new List<SignalUsage>();
        }

        public string Name { get; }
        public int Index { set; get; }
        public string CoreId { set; get; }
        public string Type { set; get; }

        public double OffsetMs { set; get; }
        public double PeriodMs { set; get; }
        public double LengthMs { set; get; }

        public List<SignalUsage> AllInputs { set; get; }
        public List<SignalUsage> AllOutputs { set; get; }

    }

    public class Signal
    {
        public Signal(string name)
        {
            Name = name;
            Readers = new List<TaskFrame>();
            Writers = new List<TaskFrame>();
        }

        public Signal(SignalSerial ss)
        {
            Name = ss.Name;
            Dim = ss.Dim;
            Size = ss.Size;
            Index = ss.Index;
            Readers = new List<TaskFrame>();
            Writers = new List<TaskFrame>();
        }

        public string Name { get; }
        public int Dim { set; get; } = 1;
        public int Size { set; get; } = 8;
        public int Index { set; get; } = 0;
        public List<TaskFrame> Readers;
        public List<TaskFrame> Writers;
    }

    public class SignalInstance
    {
        //A signal whose Dim1 > 1, it has then Dim1 number of SignalInstance.
        public SignalInstance(SignalUsage sigu, int instIndex, int seqIndex, int globalIndex, int cpuIdx)
        {
            Sigu = sigu;
            InstIndex = instIndex;
            SeqIndex = seqIndex;
            GlobalIndex = globalIndex;
            CPUIdx = cpuIdx;           
        }

        public SignalUsage Sigu { get; }
        public int GlobalIndex { get; }
        public int InstIndex { get; }
        public int SeqIndex { get; }
        public int CPUIdx { get; }
    };

    public class TaskInstance
    {
        public TaskInstance(int instindex, TaskFrame frame, int coreNum)
        {
            InstIndex = instindex;
            FrameName = frame.Name;

            ReadSignals = new List<SignalInstance>();
            WriteSignals = new List<SignalInstance>();

            ReadReadNeighbors = new List<List<TaskInstance>>();
            ReadWriteNeighbors = new List<List<TaskInstance>>();
            WriteReadNeighbors = new List<List<TaskInstance>>();
            WriteWriteNeighbors = new List<List<TaskInstance>>();

            for (int i = 0; i< coreNum; i++)
            {
                List<TaskInstance> tmp1 = new List<TaskInstance>();
                ReadReadNeighbors.Add(tmp1);

                List<TaskInstance> tmp2 = new List<TaskInstance>();
                ReadWriteNeighbors.Add(tmp2);

                List<TaskInstance> tmp3 = new List<TaskInstance>();
                WriteReadNeighbors.Add(tmp3);

                List<TaskInstance> tmp4 = new List<TaskInstance>();
                WriteWriteNeighbors.Add(tmp4);
            }

            int period = Convert.ToInt32(frame.PeriodMs * 1000);
            int offset = Convert.ToInt32(frame.OffsetMs * 1000);
            int length = Convert.ToInt32(frame.LengthMs * 1000);

            InstStart = Constants.MusToCycles(instindex * period + offset);
            InstEnd = Constants.MusToCycles(instindex * period + offset + length);

            switch (frame.CoreId)
            {
                case "A0":
                    CPUIndex = 0;
                    break;

                case "A1":
                    CPUIndex = 1;
                    break;

                case "A2":
                    CPUIndex = 2;
                    break;

                case "A3":
                    CPUIndex = 3;
                    break;

                case "A4":
                    CPUIndex = 4;
                    break;

                case "A5":
                    CPUIndex = 5;
                    break;

                default:
                    CPUIndex = -1;
                    break;
            }

        }
        public int InstIndex { get; }
        public string TaskName { get; }
        public string FrameName { get; }

        public int CPUIndex { set; get; }
        public int InstStart { set; get; }
        public int InstEnd { set; get; }

        public int MinReadStart { set; get; }
        public int MaxReadEnd { set; get; }
        public int ReadStartDelay { set; get; }
        public int ReadISRDelay { set; get; }
        public int MaxReadSyncDelay { set; get; }
        public int MaxReadRaceDelay { set; get; }
        public int MaxReadTimeCost { set; get; }

        public int MinWriteStart { set; get; }
        public int MaxWriteEnd { set; get; }
        public int WriteStartDelay { set; get; }
        public int WriteISRDelay { set; get; }
        public int MaxWriteSyncDelay { set; get; }
        public int MaxWriteRaceDelay { set; get; }
        public int MaxWriteTimeCost { set; get; }

        public List<SignalInstance> ReadSignals { set; get; }
        public List<SignalInstance> WriteSignals { set; get; }


        public List<List<TaskInstance>> ReadWriteNeighbors;
        public List<List<TaskInstance>> ReadReadNeighbors;
        public List<List<TaskInstance>> WriteReadNeighbors;
        public List<List<TaskInstance>> WriteWriteNeighbors;

    }

    public enum UsageType
    {
        Unknown,
        Optimized_Read,
        Optimized_Write,
        Optimized_ReadWrite,
        Direct_Read,
        Direct_Write,
        Direct_ReadWrite
    }

    public class SignalUsage
    {
        public SignalUsage(Signal signal, UsageType type)
        {
            Signal = signal;
            Type = type;
        }

        public Signal Signal { get; }
        public UsageType Type { get; }

    }
}
