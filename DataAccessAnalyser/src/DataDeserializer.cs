﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace DataAccessAnalyser
{
   public class DataDeserializer
    {
        public DataDeserializer(DataSet dataSet, string signalPath, string framePath)
        {
            Set = dataSet;
            FrameDict = new Dictionary<string, TaskFrame>();
            SignalDict = new Dictionary<string, Signal>();
            SignalPath = signalPath;
            FramePath = framePath;
        }

        DataSet Set;
        List<TaskFrameSerial> FrameSerials;
        List<SignalSerial> SignalSerials;
        Dictionary<string, TaskFrame> FrameDict;
        Dictionary<string, Signal> SignalDict;
        string SignalPath;
        string FramePath;

        public void JsonDeserialize()
        {
            using (var w = new StreamReader(FramePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                FrameSerials = (List<TaskFrameSerial>)serializer.Deserialize(w, typeof(List<TaskFrameSerial>));
            }

            using (var w = new StreamReader(SignalPath))
            {
                JsonSerializer serializer = new JsonSerializer();
                SignalSerials = (List<SignalSerial>)serializer.Deserialize(w, typeof(List<SignalSerial>));
            }

            foreach(TaskFrameSerial ts in FrameSerials)
            {
                TaskFrame tf = new TaskFrame(ts);
                FrameDict[tf.Name] = tf;
                Set.TaskFrames.Add(tf);
            }

            foreach(SignalSerial ss in SignalSerials)
            {
                Signal s = new Signal(ss);
                SignalDict[s.Name] = s;
                Set.Signals.Add(s);

                foreach (string reader in ss.Readers)
                {
                    s.Readers.Add(FrameDict[reader]);
                }

                foreach (string writer in ss.Writers)
                {
                    s.Writers.Add(FrameDict[writer]);
                }
            }

            foreach (TaskFrameSerial ts in FrameSerials)
            {

                TaskFrame tf = FrameDict[ts.Name];
                foreach(string sig in ts.AllInputs)
                {
                    tf.AllInputs.Add(new SignalUsage(SignalDict[sig], UsageType.Optimized_Read));
                }

                foreach (string sig in ts.AllOutputs)
                {
                    tf.AllOutputs.Add(new SignalUsage(SignalDict[sig], UsageType.Optimized_Write));
                }
            }
        }
    }
}
