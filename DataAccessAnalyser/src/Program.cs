﻿using System;
using System.Collections.Generic;
using System;
using System.IO;

namespace DataAccessAnalyser
{
    class Program
    {
        static void Main(string[] args)
        {
            string basePath = @"D:\Codes\Temp\DAA\";
            string signalPath = Path.Combine(basePath, @"Data\Signals.json");
            string framePath = Path.Combine(basePath, @"Data\Frames.json");
            string outputPath = Path.Combine(basePath, @"Result\");

            DataSet data = new DataSet();
            DataDeserializer deserializer = new DataDeserializer(data, signalPath, framePath);
            deserializer.JsonDeserialize();

            DataAccessAnalyser analyser = new DataAccessAnalyser(data, outputPath, 2000);
            analyser.Run();
            Console.WriteLine("\nThat's all for now. Press any key to exit.");
            Console.ReadKey();
        }
    }
}
