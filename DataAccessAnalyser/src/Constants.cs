﻿using System.Collections.Generic;

namespace DataAccessAnalyser
{
    public static class Constants
    {
        // freuenze von the CPU in Hz
        public static int CPU_FREQUENZE
        {
            get
            {
                return 300000000;
            }

        }

        public static List<int>[] CPU_SLI_MAPPING
        {
            get
            {
                List<int>[] sliMapping = new List<int>[2];

                List<int> d0 = new List<int>();
                d0.Add(0);
                d0.Add(1);
                d0.Add(2);
                d0.Add(3);

                List<int> d1 = new List<int>();
                d1.Add(4);
                d1.Add(5);

                sliMapping[0] = d0;
                sliMapping[1] = d1;

                return sliMapping;
            }

        }

        public static int MusToCycles(int mus)
        {
            int res = (Constants.CPU_FREQUENZE / 1000 / 1000) * mus;
            return res;
        }

        public static int CyclesToMus(long cycle)
        {
            long mus = cycle * 1000000 / Constants.CPU_FREQUENZE;
            return (int)mus;
        }

    }
}
