﻿using System;

namespace DataAccessAnalyser
{
    public static class Logger
    {
        public static void Log(string what)
        {
            Console.WriteLine(DateTime.Now + " -- " + what);
        }
    }
}
