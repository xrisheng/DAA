# Data Access Analyser

Source Code for Paper: Data Access Time Estimation in Automotive LET Scheduling with Multi-core CPU

## Dependency
.Net Framework 4.6

Newtonsoft.Json

## Usage

1. Open DataAccessAnalyser.sln in Visual Studio
2. Edit the basePath in src\Program.cs
3. Start execution
4. The result is generated in Result folder

## Contributing

Risheng Xu, Max J. Friese, Hermann von Hasseln, Dirk Nowotka